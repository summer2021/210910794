# Nav2

this is a fork of [Navigation2](https://github.com/ros-planning/navigation2)


# My work

Task1: reduce internal nodes

* The PRs for navigation2
  * [Reduce nodes for nav2_waypoint_follower #2441](https://github.com/ros-planning/navigation2/pull/2441) (merged)
  * [Reduce map saver nodes #2454](https://github.com/ros-planning/navigation2/pull/2454)  (merged)
  * [Reduce lifecycle manager nodes #2456](https://github.com/ros-planning/navigation2/pull/2456)  (merged)
  * [improve SimpleActionServer #2459](https://github.com/ros-planning/navigation2/pull/2459)  (merged)
  * [Reduce lifecycle service client nodes #2469](https://github.com/ros-planning/navigation2/pull/2469)  (merged)
  * [Reduce controller server nodes #2479](https://github.com/ros-planning/navigation2/pull/2479)  (merged)
  * [Reduce planner server nodes #2480](https://github.com/ros-planning/navigation2/pull/2480)  (merged)
  * [Reduce amcl_node nodes #2483](https://github.com/ros-planning/navigation2/pull/2483)  (blocked, under review)
  * [Reduce costmap_2d_ros nodes #2489](https://github.com/ros-planning/navigation2/pull/2489)  (blocked, under review)

* The PRs for upstream repository (`ros2/geometry2`)
  * [Reduce transform listener nodes #442](https://github.com/ros2/geometry2/pull/442) (merged)
  * [Isolate timer interface #447](https://github.com/ros2/geometry2/pull/447) (approved, under review)

Task2 : support composed bringup for Nav2 (extra task).

* The PRs for navigation2
  * [Fix export dependency and library #2521](https://github.com/ros-planning/navigation2/pull/2521) (merged)
  * [Add argument node options #2522](https://github.com/ros-planning/navigation2/pull/2522)  (merged)
  * [Support manual composed bringup for Nav2 applications #2555](https://github.com/ros-planning/navigation2/pull/2555) (under review)
  * [Register nodes as components #2562](https://github.com/ros-planning/navigation2/pull/2562)  (merged)
  * [Github branch for dynamic composed bringup](https://github.com/gezp/navigation2/tree/dynamic_composed_bringup) (a new pull request will be created, blocked by some issues)

* The PRs for upstream repository (`ros2/rclcpp`) 
  * [add use_global_arguments for node options of component nodes #1776](https://github.com/ros2/rclcpp/pull/1776) (under review)
  * [support dedicated executor for component nodes #1781](https://github.com/ros2/rclcpp/pull/1781) (under review)

you can find more details of PRs for navigation2 in `branchs` except `main`.
